/*
	Date: 2/5/2020
	Name Part 1: Bob Baitinger
	Project: playingCards part 1

Write a C++ program that utilizes enumerations to create playing cards.
Part 1:
	Create a program that has a structure for playing cards.
	Call the struct "Card."
	The rank and suit should be enumerations.
	Note : an Ace should be a high card with a value of 14.
	Create a PUBLIC repository and upload your project to Bitbucket.
	Notify your instructor when your project is uploaded.


Part 2 :
	You will be continuing the lab exercise using a partners project(part 1), and they will use yours.
	Fork your partners repositoryand add the following functions :
	void PrintCard(Card card)
	Prints out the rankand suit of the card.Ex : The Queen of Diamonds
	Card HighCard(Card card1, Card card2)
	Determines which of the two supplied cards has the higher rank.
	Commitand push your changes to Bitbucket.
	Submit the link to your repository in Blackboard.
*/

enum _suit
{
	SUIT_HEARTS_ENUM,
	SUIT_DIAMONDS_ENUM,
	SUIT_CLUBS_ENUM,
	SUIT_SPADES_ENUM,
};

enum _rank
{
	RANK_TWO_ENUM = 2,
	RANK_THREE_ENUM,
	RANK_FOUR_ENUM,
	RANK_FIVE_ENUM,
	RANK_SIX_ENUM,
	RANK_SEVEN_ENUM,
	RANK_EIGHT_ENUM,
	RANK_NINE_ENUM,
	RANK_TEN_ENUM,
	RANK_JACK_ENUM,
	RANK_QUEEN_ENUM,
	RANK_KING_ENUM,
	RANK_ACE_ENUM,
};

struct card
{
	_suit suit;
	_rank rank;
};

int main(void)
{

	card currentCard;
	currentCard.rank = RANK_ACE_ENUM;
	currentCard.suit = SUIT_DIAMONDS_ENUM;

	return 0;
}